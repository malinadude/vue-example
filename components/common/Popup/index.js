import Popup from './Popup';
import PopupGallery from './PopupGallery';

export { Popup, PopupGallery };
