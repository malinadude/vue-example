const defaultBreadcrumbs = Object.freeze({
  ru: [
    {
      name: 'Главная',
      slug: '/',
      opening: true,
    },
  ],
  en: [
    {
      name: 'Home',
      slug: '/',
      opening: true,
    },
  ],
});

export const actions = {
  async setBreadcrumbs({ commit }, { breadcrumbs }) {
    return commit('SET_BREADCRUMBS', breadcrumbs);
  },
  async clearBreadcrumbs({ commit }) {
    return commit('CLEAR_BREADCRUMBS');
  },
};

export const mutations = {
  SET_BREADCRUMBS(state, breadcrumbs) {
    state.links = defaultBreadcrumbs[this.$i18n.locale].concat(breadcrumbs);

    return state.links;
  },
  CLEAR_BREADCRUMBS(state) {
    state.links = [];

    return state.links;
  },
};

export const state = () => ({
  links: [],
});

export default {
  state,
  actions,
  mutations,
};
