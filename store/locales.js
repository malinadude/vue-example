import { LocalesApiService } from '~/services/common';

export const actions = {
  async setActiveLocale({ commit, state }, currentLocale) {
    const activeLocale = state.locales.find(
      (locale) => locale.code === currentLocale,
    );

    if (activeLocale) {
      return commit('SET_ACTIVE_LOCALE', activeLocale);
    }

    return commit(
      'SET_ACTIVE_LOCALE',
      this.$i18n.locales.find((locale) => locale.code === currentLocale),
    );
  },
};

export const mutations = {
  SET_ACTIVE_LOCALE(state, locale) {
    state.activeLocale = locale;

    return state.activeLocale;
  },
};

export const state = () => ({
  locales: LocalesApiService.getLocales(),
  activeLocale: {},
});

export default {
  state,
  actions,
  mutations,
};
