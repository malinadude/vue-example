export const actions = {
  async setPageTitle({ commit }, { pageTitle }) {
    return commit('SET_PAGE_TITLE', pageTitle);
  },
};

export const mutations = {
  SET_PAGE_TITLE(state, pageTitle) {
    state.pageTitle = pageTitle;

    return state.pageTitle;
  },
};

export const state = () => ({
  pageTitle: '',
});

export default {
  state,
  actions,
  mutations,
};
