export const actions = {
  async screenCheck({ state, commit }) {
    const screen = window.innerWidth;
    let currentDevice = null;
    let targetDevice = null;

    Object.keys(state.devices).forEach((device) => {
      if (state.devices[device]) {
        currentDevice = device;
      }
    });

    if (screen <= state.screen.xs) {
      targetDevice = 'mobile';
    } else if (screen <= state.screen.sm) {
      targetDevice = 'tablet';
    } else if (screen <= state.screen.md) {
      targetDevice = 'laptop';
    } else if (screen > state.screen.md) {
      targetDevice = 'desktop';
    }

    if (currentDevice !== targetDevice) {
      if (currentDevice) {
        commit(`SCREEN_CHECK_${currentDevice.toUpperCase()}`, false);
      }

      return commit(`SCREEN_CHECK_${targetDevice.toUpperCase()}`, true);
    }
  },
};

export const mutations = {
  SCREEN_CHECK_MOBILE(state, handle) {
    state.devices.mobile = handle;

    return state.devices.mobile;
  },
  SCREEN_CHECK_TABLET(state, handle) {
    state.devices.tablet = handle;

    return state.devices.tablet;
  },
  SCREEN_CHECK_LAPTOP(state, handle) {
    state.devices.laptop = handle;

    return state.devices.laptop;
  },
  SCREEN_CHECK_DESKTOP(state, handle) {
    state.devices.desktop = handle;

    return state.devices.desktop;
  },
};

export const state = () => ({
  screen: {
    xs: 768,
    sm: 1024,
    md: 1200,
  },
  devices: { mobile: false, tablet: false, laptop: false, desktop: false },
});

export default {
  state,
  actions,
  mutations,
};
