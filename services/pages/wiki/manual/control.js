import Base from '~/services/base';

export default class ManualApiService extends Base {
  async initCategories({ locale }) {
    const categories = await this.axios.$get(`/manual-categories`, {
      params: {
        locale,
      },
    });

    return categories.map((category) => {
      return {
        full_latin_name: category.full_latin_name,
        latin_name: category.latin_name,
        name: category.name,
        icon: category.icon,
        items: category.manuals.map((manual) => {
          return {
            full_latin_name: manual.full_latin_name,
            latin_name: manual.latin_name,
            name: manual.title,
          };
        }),
      };
    });
  }

  async initPost({ category, locale }) {
    return this.axios.$get(`/manual-categories/${category}/detail`, {
      params: {
        locale,
      },
    });
  }

  async searchCategories({ query, locale }) {
    return this.axios.$get(`/manual-categories/search`, {
      params: {
        locale,
        query,
      },
    });
  }

  async searchPosts({ query, locale }) {
    return this.axios.$get(`/manuals/search`, {
      params: {
        locale,
        query,
      },
    });
  }

  async postRate({ type, post }) {
    return this.axios.$post(
      `/manual-categories/${post}/rate`,
      {
        type,
      },
      {
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
  }
}
